"""
This Module contains a function which is used in the main class of the project.
The function can be used to enumerate a series of calendarweeks that spans
several years. The function is used within the Bicycle class' plot_week method.

    Functions

    ----------
    enumerate_weeks(vals)
        Take an array of calendar weeks and convert them into unique week numbers.
        This is useful if a time series spans several years, hence the calendarweek
        would repeat itself.
"""


def enumerate_weeks(vals):
    """
    Parameters
    --------------
    vals : Array or array-like object
        an array of calendar weeks


    Returns
    -------------
    new_vals : List
        an array of converted calendar weeks to unique calendarweeks
        starting from the beginning of the time series.
    """

    n_initial = -1
    current_week = 0
    new_vals = []
    for week in vals:
        if week == current_week:
            new_vals.append(n_initial)
        else:
            current_week = week
            n_initial += 1
            new_vals.append(n_initial)

    return new_vals
