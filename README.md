### Project Title:
- The Bike Renatals Analysis, 2012
- The project was created as a group work as part of the Advanced Programming for Data Science Class, Nova SBE, 2021.

### Prerequisites:
The list of imports requrired to proceed with the project and load the code:
- import numpy as np #Python library used for working with arrays
- import pandas as pd #software library written for data manipulation and analysis.
- from zipfile import ZipFile #to allow for work with ZIP archives
- import sys #system-specific parameters and functions
- from io import BytesIO #data can be kept as bytes in an in-memory buffer when we use the io module’s Byte IO operations
- import requests #Use pip install requests as it is not a built-in Python module; datasets HTTTP library
- import os #module to interact with the underlying operating syste
- import matplotlib.pyplot as plt #library for creating static, animated, and interactive visualizations
- import seaborn as sns #library for making statistical graphics; built on top of matplotlib; integrated with pandas data structures
- import datetime #module suppling classes for manipulating dates and times
- sys.path.append('../classes') #function which appendeds to include new file paths that will point to modules the user wants to import
- from bicycles import Bicycles #the dataset based on which the project is created


### Running the tests:
- Pylint is a Python static code analysis tool which looks for programming errors, helps enforcing a coding standard, sniffs for code smells and offers simple refactoring suggestions.
- Run locally; style score: 10/10, module functions: 9/10 (use pip install requests for requests to work on your computer)


### Authors: 
- AlaEddine Khattat, 44827@novasbe.pt
- Claudio Conte, 46432@novasbe.pt
- Constanza Suriano, 44029@novasbe.pt
- Felix Julian Pagel, 44727@novasbe.pt
- Muhammad Abdullah Boqa, 45913@novasbe.pt
- Weronika Dyczek, 44622@novasbe.pt


### License: 
This project is licensed under the MIT License - see the LICENSE.md file for details
