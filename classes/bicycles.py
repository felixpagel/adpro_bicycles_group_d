"""
This Module contains a class that helps in handling the data for the
Bicycles hackathon.

    Classes

    ----------
    Bicycles()
        Helpy in handling the project data, meaning loading, ploting weeks
        and looking at correlations.
"""

from zipfile import ZipFile
import sys
from io import BytesIO
import datetime
import os
import calendar
import requests
import matplotlib.pyplot as plt
import seaborn as sns
# import pylab
import pandas as pd
#import numpy as np
# from scipy import stats
sys.path.append('../functions')
from functions import enumerate_weeks


class Bicycles:
    """
    This class helps to handle the bicycles hackathon data,
    namely the loading and plotting of the data.

    ...

    Attributes

    ----------

    data : DataFrame
        Contains the DataFrame with the bicycles data

    ----------

    state : string
        Describes the current status of the class, meaning wheter data has been loaded.

    ----------

    ...

    Methods

    ----------

    download()
        Downloads the .zip file with the data into a downloads/ directory in the main
        project directory. The .zip file will be automatically unzipped into the
        downloads/ directory leaving two .csv files. If the .csv data file already
        exists in that directory,the method will not download the .zip file again.

    ----------

    read()
        Reads the .csv file with hourly aggregations from the original .zip file in
        the /downloads directory into a pandas dataframe which will become the attribute
        self.data of your Class.

   ----------

   correlation_matrix()
        Plots a correlation matrix of month,humidity,weather situation, temperature,
        windspeed and total number of bike rentals of the data contained in the
        self.data attribute.

   ----------

    plot_week(week: int)
         Allows to pick a week number between 0 and 102 weeks then plot the daily aggregated
         number of bike rentals for the chosen week.
         Reference is the first week of the dataset.
         If possible, the plot will always contain the data from Monday to Sunday.

    ----------

    visualizing_average_count()

        This method plots the average uses count by month(january,february,etc.)
        and by hour of the day across season(fall,summer,spring,winter) and across
        user type(registered,casual)

    ----------

    forecast(month: str)
         Takes a month in string form as input and return a plot of a 24-hour period
         corresponding to the average rental (shaded area represents the standard
         deviation interval) in that month for each weekday

    """

    def __init__(self):
        self.state = "empty"
        self.data = None

    def download(self):
        """

        Returns : None
        ---------------
        """
        # changing directory to files folder
        d_cur = os.getcwd()
        os.chdir('..')
        os.chdir('downloads')

        try:
            self.data = pd.read_csv("hour.csv")
            self.data["timestamp"] = self.data.dteday + self.data.hr.apply(lambda x:
                                                                        str(x).zfill(2))
            self.data["timestamp"] = self.data.timestamp.apply(lambda x:
                datetime.datetime(int(x[:4]), int(x[5:7]), int(x[-4:-2]), hour = int(x[-2:])))
            self.data.index = pd.DatetimeIndex(data = self.data["timestamp"])
            self.data["calendarweek"] = self.data.index.isocalendar().week
            self.data["mnth_categorical"] = self.data.timestamp. \
                            apply(lambda date : calendar.month_name[date.month])
            self.data["season"] = self.data.season.map({1: "Spring", 2 : "Summer", 3 : "Fall",
                                                        4 :"Winter" })
            self.data["weathersit_categorical"] = self.data.weathersit.map({
                    1: " Clear + Few clouds + Partly cloudy + Partly cloudy",\
                    2 : " Mist + Cloudy, Mist + Broken clouds, Mist + Few clouds, Mist ", \
                    3 : """ Light Snow, Light Rain + Thunderstorm + Scattered clouds,
                                                        Light Rain + Scattered clouds""", \
                    4 : " Heavy Rain + Ice Pallets + Thunderstorm + Mist, Snow + Fog " })
            #Coercing To Category Type
            category_variable_list = ["hr","mnth_categorical","weekday","season",
                                      "weathersit_categorical","holiday","workingday"]
            for var in category_variable_list:
                self.data[var] = self.data[var].astype("category")

        except FileNotFoundError:
            resp = requests.get("""https://archive.ics.uci.edu/ml/machine-learning-databases/00275/
                                                            Bike-Sharing-Dataset.zip""")

            z_file = ZipFile(BytesIO(resp.content))
            z_file.extractall()
            self.data = pd.read_csv("hour.csv")
            self.data["timestamp"] = self.data.dteday + self.data.hr.apply(lambda x:
                                                                           str(x).zfill(2))
            self.data["timestamp"] = self.data.timestamp.apply(lambda x:
                               datetime.datetime(int(x[:4]), int(x[5:7]), int(x[-4:-2]),
                                                                    hour = int(x[-2:])))
            self.data.index = pd.DatetimeIndex(data = self.data["timestamp"])
            self.data["calendarweek"] = self.data.index.isocalendar().week
            self.data["mnth_categorical"] = self.data.timestamp. \
                            apply(lambda date : calendar.month_name[date.month])
            self.data["season"] = self.data.season.map({1: "Spring", 2 : "Summer",
                                                        3 : "Fall", 4 :"Winter" })
            self.data["weathersit_categorical"] = self.data.weathersit.map({
                    1: " Clear + Few clouds + Partly cloudy + Partly cloudy",\
                    2 : " Mist + Cloudy, Mist + Broken clouds, Mist + Few clouds, Mist ", \
                    3 : """ Light Snow, Light Rain + Thunderstorm + Scattered clouds,
                                                    Light Rain + Scattered clouds""", \
                    4 :" Heavy Rain + Ice Pallets + Thunderstorm + Mist, Snow + Fog " })
            #Coercing To Category Type
            category_variable_list = ["hr","mnth_categorical","weekday","season",
                                      "weathersit_categorical","holiday","workingday"]
            for var in category_variable_list:
                self.data[var] = self.data[var].astype("category")
            self.state = "data loaded"

        else:
            self.state = "data loaded"

        finally:
            # set back working directory
            os.chdir(d_cur)

    def read(self):
        """

        Returns : None
        ---------------

        """
        # changing directory to files folder
        d_cur = os.getcwd()
        os.chdir('..')
        os.chdir('downloads')
        try:
            self.data = pd.read_csv("hour.csv")
            self.data["timestamp"] = self.data.dteday + self.data.hr.apply(lambda x:
                                                                        str(x).zfill(2))
            self.data["timestamp"] = self.data.timestamp. \
                            apply(lambda x: datetime.datetime(int(x[:4]), int(x[5:7]),
                                                    int(x[-4:-2]), hour = int(x[-2:])))
            self.data.index = pd.DatetimeIndex(data = self.data["timestamp"])
            self.data["calendarweek"] = self.data.index.isocalendar().week
            self.data["mnth_categorical"] = self.data.timestamp. \
                            apply(lambda date : calendar.month_name[date.month])
            self.data["season"] = self.data.season.map({1: "Spring", 2 : "Summer",
                                                        3 : "Fall", 4 :"Winter" })
            self.data["weathersit_categorical"] = self.data.weathersit.map({
                    1: " Clear + Few clouds + Partly cloudy + Partly cloudy",\
                    2 : " Mist + Cloudy, Mist + Broken clouds, Mist + Few clouds, Mist ", \
                    3 : """Light Snow, Light Rain + Thunderstorm + Scattered clouds,
                                                    Light Rain + Scattered clouds""", \
                    4 :" Heavy Rain + Ice Pallets + Thunderstorm + Mist, Snow + Fog " })
            #Coercing To Category Type
            category_variable_list = ["hr","mnth_categorical", "weekday","season",
                                    "weathersit_categorical","holiday","workingday"]
            for var in category_variable_list:
                self.data[var] = self.data[var].astype("category")
        except FileNotFoundError:
            print("No file to read, use dowload method first")

        finally:
            # set back working directory
            os.chdir(d_cur)

    def correlation_matrix(self):
        """

        Returns : None
        -------------

        """
        # Note: in the task description it says 'situation', but since there's no column
        # with this name we chose 'season'.
        df_small = self.data.loc[:, ["mnth", "hum", "weathersit", "temp", "windspeed", "cnt"]]
        correlation_mat = df_small.corr()
        sns.heatmap(correlation_mat)
        plt.show()

    def visualizing_average_count(self):
        """

        Parameters
        --------------
            none

        Returns
        -------------
            plots the average total rentals by month of the year, by hour of the day and by
            hour of the day in different seasons

        """

        fig,(ax1,ax2,ax3)= plt.subplots(nrows=3)
        fig.set_size_inches(12, 20)
        sort_order = ["January","February","March","April","May","June","July",
                      "August","September","October","November","December"]
#        hueOrder = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

        month_aggregated = pd.DataFrame(self.data.groupby("mnth_categorical")["cnt"]
                                                                        .mean()).reset_index()
        month_sorted = month_aggregated.sort_values(by="cnt",ascending=False)
        sns.barplot(data=month_sorted,x="mnth_categorical",y="cnt",ax=ax1,order=sort_order)
        ax1.set(xlabel='Month', ylabel='Avearage Count',title="Average Count By Month")

        hour_aggregated = pd.DataFrame(self.data.groupby(["hr","season"],sort=True)["cnt"]
                                                                        .mean()).reset_index()
        sns.pointplot(x=hour_aggregated["hr"], y=hour_aggregated["cnt"],
                      hue=hour_aggregated["season"], data=hour_aggregated, join=True,ax=ax2)
        ax2.set(xlabel='Hour Of The Day', ylabel='Users Count',
                title="Average Users Count By Hour Of The Day Across Season",label='big')

        hour_transformed = pd.melt(self.data[["hr","casual","registered"]], id_vars=['hr'],
                                   value_vars=['casual', 'registered'])
        hour_aggregated = pd.DataFrame(hour_transformed.groupby(["hr","variable"],
                                            sort=True)["value"].mean()).reset_index()
        sns.pointplot(x=hour_aggregated["hr"], y=hour_aggregated["value"],
                      hue=hour_aggregated["variable"],hue_order=["casual","registered"],
                      data=hour_aggregated, join=True,ax=ax3)
        ax3.set(xlabel='Hour Of The Day', ylabel='Users Count',
                title="Average Users Count By Hour Of The Day Across User Type",label='big')

    def plot_week(self, week: int):
        """

        Parameters
        --------------
        Take a week number between 0 and 102

        Raises
        ---------------
        ValueError
            if the chosen number is not in the [0,102] range.

        Returns : None
        -------------

        """
        if (week >= 0) & (week <= 102):

            df_small = self.data \
                .groupby(self.data.index) \
                .sum()
            df_small["weeknumber"] = enumerate_weeks(df_small.index.isocalendar().week)
            filtered = df_small[df_small.weeknumber == week]
            plt.figure(figsize = (12, 7))
            plt.bar(list(range(len(filtered.cnt))), filtered.cnt, width = 1)
            plt.xlabel("Hour of week (Start Monday 00:00)")
            plt.ylabel("Rental count per hour")
            plt.title(f"Week {week}:  {filtered.index.date[0]} - {filtered.index.date[-1]}")
            plt.xticks(list(range(12, 157, 24)), ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"])
            plt.show()

        else:
            raise ValueError

    def forecast(self, month: str):
        """

        Parameters
        --------------
            takes a month expressed as a string

        Raises exception
        ---------------
            if the input value is anything different than a string and/or not
            belonging to the set {'January', 'February'...}

        Returns
        -------------
            plot of a 24-hour period corresponding to the average rental (shaded area
            represents the standard deviation interval)

        """

        if not (isinstance(month, str) and month in {'January', 'February', 'March', 'April',
                'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'}):
            raise Exception(
                "inserted argument must be a string expressing a month (e.g. 'January')")

        df_grouped = self.data[self.data['mnth_categorical'] == month].groupby(by=['weekday', 'hr'],
                                         as_index = False).cnt.agg(['mean', 'std']).reset_index()

        support_dict = {0: [df_grouped[df_grouped['weekday']==0], 'Monday'],
                        1: [df_grouped[df_grouped['weekday']==1], 'Tuesday'],
                        2: [df_grouped[df_grouped['weekday']==2], 'Wednesday'],
                        3: [df_grouped[df_grouped['weekday']==3], 'Thursday'],
                        4: [df_grouped[df_grouped['weekday']==4],'Friday'],
                        5: [df_grouped[df_grouped['weekday']==5], 'Saturday'],
                        6: [df_grouped[df_grouped['weekday']==6], 'Sunday']}

        fig, axs = plt.subplots(figsize=(13,30), nrows=7)
        fig.subplots_adjust(hspace=.5)
        fig.text(0.06, 0.5, 'Average rentals', ha='center', va='center',
                             rotation='vertical', fontsize='xx-large')
        fig.suptitle('Average rentals per day of the week in {}'.format(month), y=0.9,
                                                                         fontsize='xx-large')

        colors = plt.rcParams["axes.prop_cycle"]()
        for i in range(7):
            sns.pointplot(x=support_dict[i][0]['hr'], y=support_dict[i][0]['mean'],
                        color = next(colors)["color"], ax=axs[i])
            axs[i].fill_between(support_dict[i][0]['hr'],
                                support_dict[i][0]['mean']-support_dict[i][0]['std'],
                                support_dict[i][0]['mean']+support_dict[i][0]['std'],
                                alpha=0.2, color = next(colors)["color"])
            axs[i].set(xlabel='{}'.format(support_dict[i][1]), ylabel = None, label='small')
